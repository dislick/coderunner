(function(context) {

  Object.defineProperty(context, 'CodeRunner', {
    writeable: false,
    value: function(config) { 
      if (!config) {
        var config = {};
      }
      this.options = {
        autorun: config.autorun || false
      }
    }
  });

  var CodeRunner = context.CodeRunner;

  // accepts a selector and loops through every element
  CodeRunner.prototype.makeExecutable = function(selector) {
    var nodes = document.querySelectorAll(selector);

    for (var indexNodes = 0; indexNodes < nodes.length; indexNodes++) {
      this._run(nodes[indexNodes]);
    }
  };

  CodeRunner.prototype._run = function(element) {
    // transform html
    this._transformHtml(element);

    // add event listeners
    this._addEventListeners(element);
  };

  // inserts a result-wrapper after the element which holds the run button
  CodeRunner.prototype._transformHtml = function(element) {
    var additionalMarkup = '<textarea spellcheck="false" class="edit"></textarea><div class="result-wrapper"><input type="button" value="Run" /><div class="result"></div></div>';
    var wrapper = document.createElement('div');
    wrapper.innerHTML = additionalMarkup;
    element.parentNode.insertBefore(wrapper, element.nextSibling);
  };

  // reacts to a click event on the run button
  CodeRunner.prototype._addEventListeners = function(element) {
    var root = this;
    var resultWrapper = element.nextElementSibling;
    var runButton = resultWrapper.querySelector('input');
    var result = resultWrapper.querySelector('.result');
    var textarea = resultWrapper.querySelector('textarea.edit');

    var clickEvent = function() {
      var code = element.innerText;
      var evaluatedCode = runCode(code);
      result.innerHTML = evaluatedCode;
      runButton.style.display = 'none';
    };

    runButton.addEventListener('click', clickEvent);
    element.addEventListener('click', function() {
      root._activateEditMode(element, textarea, runButton, result);
    });

    if (this.options.autorun) {
      clickEvent();
    }
  };

  CodeRunner.prototype._activateEditMode = function(element, textarea, runButton, result) {
    var root = this;
    var elementHeight = element.clientHeight;
    textarea.style.display = 'block';
    element.style.display = 'none';
    textarea.value = element.innerText;
    textarea.style.height = elementHeight + 2 + 'px';
    textarea.focus();
    this._reactivateRunButton(runButton, result);

    textarea.addEventListener('blur', function() {
      root._deactivateEditMode(element, textarea);
    });
  };

  CodeRunner.prototype._deactivateEditMode = function(element, textarea) {
    textarea.style.display = 'none';
    element.style.display = 'block';
    element.innerText = textarea.value;

    // run prettyPrint if defined
    if (window.prettyPrint) {
      prettyPrint();
    }
  };

  CodeRunner.prototype._reactivateRunButton = function(runButton, result) {
    runButton.style.display = 'block';
    result.innerText = '';
  };

  // can log a single statement even without console.log in user code
  var wrapConsoleLog = function(code) {
    if (code.indexOf('console.log(') == -1) {
      code = 'console.log(' + code + ');';
    }
    return code;
  };

  // runs code by overwriting the console.log method
  // after it's finished, it sets console.log back to the original function
  var runCode = function(code) {
    var originalLog = console.log;
    var log = new String();
    console.log = function(text) {
      try {
        text = JSON.stringify(text);
      } catch (exception) {
        console.error(exception);
      }
      log += '<p><span class="brighter">&gt; </span>' + text + '</p>';
      originalLog.apply(this, arguments);
    };

    try {
      new Function(wrapConsoleLog(code))();  
    } catch (exception) {
      log += '<p class="error"><span class="brighter">&gt; </span>' + exception + '</p>';
    }
    
    console.log = originalLog;
    return log;
  };

})(this);
